/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CThread class.
 */
/*----------------------------------------------------------*/
#ifndef AUTOLOCK_H
#define	AUTOLOCK_H

#include <pthread.h>
#include <assert.h>

/*----------------------------------------------------------*/
/*! \brief Helper class to deal with automatic Mutexes.
 */
/*----------------------------------------------------------*/
class CAutoLock
{
    pthread_mutex_t *m_Mutex;
    
public:
    CAutoLock(pthread_mutex_t *pMutex) : m_Mutex(NULL)
    {
        assert(NULL != pMutex);
        m_Mutex = pMutex;
        pthread_mutex_lock( m_Mutex );
    }
    virtual ~CAutoLock()
    {
        pthread_mutex_unlock( m_Mutex );
    }
};

#endif

