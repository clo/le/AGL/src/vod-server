/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file generates the start page of the document you are currently reading.
 */
/*----------------------------------------------------------*/
#ifndef DOXYGEN_START_PAGE_H
#define DOXYGEN_START_PAGE_H


/*----------------------------------------------------------*/
/*! \mainpage
 *
 * \section intro_sec Introduction
 *
 * The client device may have MLB attached INIC OS81110 or USB attached INIC OS81118. \n
 * All components where written for and tested for the FreeScale i.MX6q platform. \n
 *
 * \subsection step1 Step 1: Execute Server Application
 *
 * Login to the server target device. \n
 * Switch to the directory, where you have copied all outputs of the previous steps. \n
 * Make sure, that all devices are up and running and the MOST bus is well connected. \n
 * Start the application. \n
 * Enter:
 * \code sudo ./VideoOnDemand config.xml \endcode
 *
 * After a while, you should see the main menu, with available connections: \n
 * Display Output: \n
 * \code
 * Index | MOST | MAC-ADDRESS       | DEV  | INST | ID | Type  | BW  | Dir | Multiplex | Con-Label | Buffer | State  |DEVICE-NAME 
 * ------|------|-------------------|------|------|----|-------|-----|-----|-----------|-----------|--------|--------|-------------
 * 00    | 00   | 02-00-00-00-00-01 | 310  | 00   | 01 | ISOC  | 088 | TX  | Yes       | 077       | 8192   | XXX    | /dev/mdev0-ep0a
 * 01    | 00   | 02-00-00-00-00-01 | 310  | 00   | 02 | ISOC  | 088 | TX  | Yes       | 0CF       | 8192   | XXX    | /dev/mdev0-ep0b
 * 02    | 00   | 02-00-00-00-00-01 | 310  | 00   | 03 | ISOC  | 088 | TX  | Yes       | 127       | 8192   | XXX    | /dev/mdev0-ep0c
 * 03    | 00   | 02-00-00-00-00-01 | 310  | 00   | 04 | ISOC  | 088 | TX  | Yes       | 17F       | 8192   | XXX    | /dev/mdev0-ep0d
 * 04    | 00   | 02-00-00-01-01-01 | 330  | 00   | 01 | ISOC  | 088 | RX  | No        | 077       | None   | XXX    | 
 * 05    | 00   | 02-00-00-02-01-02 | 330  | 01   | 01 | ISOC  | 088 | RX  | No        | 0CF       | None   | XXX    | 
 * ------|------|-------------------|------|------|----|-------|-----|-----|-----------|-----------|--------|--------|-------------
 * \endcode
 *
 * At this point you should be able to connect with either the native C++ application or the Android HMI app.<br/>
 * Make sure, you set the IP address correct on all devices.
 * 
 * - In order to check if the streaming is correctly working, enable the the statistics printing.
 * - Enter "p" and press Enter key.
 *
 * Display Output:
 * \code
 * ==============Statistic Start==============
 * Device:/dev/mdev0-ep0a, Stream 02-00-00-00-00-01, sent:0 (kBit/s), stuffed:28700 (kBit/s),  read:0 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Device:/dev/mdev0-ep0b, Stream 02-00-00-00-00-01, sent:0 (kBit/s), stuffed:28730 (kBit/s),  read:0 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Device:/dev/mdev0-ep0c, Stream 02-00-00-00-00-01, sent:0 (kBit/s), stuffed:33627 (kBit/s),  read:0 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Device:/dev/mdev0-ep0d, Stream 02-00-00-00-00-01, sent:0 (kBit/s), stuffed:33627 (kBit/s),  read:0 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Device:/dev/mdev0-ep0a, Stream 02-00-00-01-01-01, sent:4927 (kBit/s), stuffed:0 (kBit/s),  read:5197 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Device:/dev/mdev0-ep0b, Stream 02-00-00-02-01-02, sent:4897 (kBit/s), stuffed:0 (kBit/s),  read:5005 (kBit/s), PCR-error:0, Buf Ovfl-error:0, Buf Unfl-error:0, TS-error:0
 * Overall sent performance of all streams: 134511 (kBit/s)
 * ===============Statistic End===============
 * \endcode
 * 	- If everything is okay, the sent and read values are not zero. They may be different, as the Transport Stream file usually contains
 *    stuffing and control informations, which will not be sent over the MOST network.
 * 	- The error values must be zero.
 *
 *
 * \subsection step2 Step 2: Retrieve the isochronous stream on non Android clients
 *
 * If you are not using the Android App, login in to the client devices.\n
 * \note Make sure you performed step
 *   - Step 1: Load Linux driver on target
 *   - Step 2: Enable Ethernet communication on MOST
 *   - Step 3: Execute the native C++ VideoOnDemand Client sample application
 *   
 *
 *
 * In order to dump the received data to disk, enter:
 * \code
 * dd if=/dev/mdev0-ep0a of=myCapturedVideo.ts bs=7520
 * \endcode
 * \note Adapt the name of the character device, as listed by the VideoOnDemand sample application.
 * 
 * You can also use the dev2udp helper tool, which is stored in the SlimCamera example folder.
 */
/*----------------------------------------------------------*/



#endif //DOXYGEN_START_PAGE_H
