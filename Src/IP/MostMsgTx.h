/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CMostMsgTx class.
 */
/*----------------------------------------------------------*/
#ifndef MOSTMSGTX_H
#define MOSTMSGTX_H

#include <stdint.h>
#include <stdbool.h>
#include "MostMsg.h"
#include "MsgAddr.h"




/*----------------------------------------------------------*/
/*!
* \brief  class holding a MOST message which needs to be
*         sent
*/

/*----------------------------------------------------------*/
class CMostMsgTx : public CMostMsg
{
private:
    typedef void ( *OnMessageSent_t )( bool bSucceeded, CMostMsgTx *Msg );
    CMsgAddr *m_Addr;
    uint32_t m_nTimeoutMs;
    OnMessageSent_t m_MessageSent;
    void *m_UserContext;

protected:
    virtual ~CMostMsgTx();

public:


    /*----------------------------------------------------------*/
    /*! \brief constructs a new message
    *
    * \param Addr - Target address
    * \param nFBlock - Function Block ID
    * \param nInst - Instance ID
    * \param nFunc - Function ID
    * \param nOpType - Operation
    * \param nPayloadLen - length of payload in bytes
    * \param Payload - Payload
    * \param nTimeoutMs - Timeout for sending in Milliseconds
    * \param MessageSentCB - Handler called, after message was sent
    * \param UserContext - a class passed to the handler
    */
    /*----------------------------------------------------------*/
    CMostMsgTx( CMsgAddr *Addr, uint32_t nFBlock, uint32_t nInst, uint32_t nFunc, uint8_t nOpType,
        uint32_t nPayloadLen, const uint8_t *Payload, uint32_t nTimeoutMs, OnMessageSent_t MessageSentCB,
        void *UserContext );


    void MsgSent( bool bSuccess );




    CMsgAddr *GetAddr();




    void *GetUserContext();
};


#endif //MOSTMSGTX_H
