/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include <DoxyGenStartPage.h>
#include "Console.h"
#include "VodHandler.h"

static void PrintMenu()
{
    ConsolePrintfStart( PRIO_HIGH, "Main Menu, press key and enter:\n" );
    ConsolePrintfContinue( "-------------------------------\n" );
    ConsolePrintfContinue( " m - Print this menu\n" );
    ConsolePrintfContinue( " r - Toggle periodically statistics printing\n" );
    ConsolePrintfContinue( " s - Stream multiple streams to fake sinks\n" );
    ConsolePrintfContinue( " x - Exit this application\n" );
    ConsolePrintfExit( "-------------------------------\n" );
}

int main( int argc, char *argv[] )
{
    ConsoleInit( false );

    char *searchPath = NULL;
    bool disableMenu = true;
    ConsolePrio_t prio = PRIO_HIGH;
    
    ConsolePrintf( PRIO_HIGH, BLUE"========== Video On Demand Server Start =========="RESETCOLOR"\n" );
    CVodHandler *mainClass = CVodHandler::GetInstance();

    for( int32_t i = 1; i < argc; i++ )
    {
        if( strstr( argv[i], "-p" ) )
        {
            if( argc <= ( i + 1 ) )
            {
                ConsolePrintf( PRIO_ERROR, RED"-p parameter requires search path as next parameter"RESETCOLOR"\n" );
                return -1;
            }
            searchPath = argv[i + 1];
            i++;
        }
        else if( strstr( argv[i], "-i" ) )
        {
            if( argc <= ( i + 1 ) )
            {
                ConsolePrintf( PRIO_ERROR, RED"-i parameter requires string fragment which shall be ignored"RESETCOLOR"\n" );
                return -1;
            }
            mainClass->SetCdevIgnorePattern(argv[i + 1]);
            i++;
        }
        else if( strstr( argv[i], "-m" ) )
        {
            disableMenu = false;
        }
        else if( strstr( argv[i], "-vv" ) )
        {
            prio = PRIO_LOW;
        }
        else if( strstr( argv[i], "-v" ) )
        {
            prio = PRIO_MEDIUM;
        }
        else if( strstr( argv[i], "--help" ) )
        {
            ConsolePrintfStart( PRIO_HIGH, "Usage: %s [OPTION] ... [FILE] ... \n", argv[0] );
            ConsolePrintfContinue( "Example MOST VideoOnDemand streaming service.\n" );

            ConsolePrintfContinue( "\t-p\t\t path to search the media files."\
                " If not set, the callee path will be searched\n" );
            ConsolePrintfContinue( "\t-m\t\t enable user menu\n" );
            ConsolePrintfContinue( "\t-i\t\t ignore all cdev with the given string fragments\n" );
            ConsolePrintfContinue( "\t-v\t\t verbose mode - print debug info\n" );
            ConsolePrintfContinue( "\t-vv\t\t super verbose mode - print even more debug info\n" );

            ConsolePrintfContinue( "Example: %s -p /root\n", argv[0] );
            return 0;
        }
        else
        {
            ConsolePrintf( PRIO_ERROR, RED
                "Ignoring command line parameter at pos %d: %s, type '%s --help'"\
                " to see the correct parameter syntax"RESETCOLOR\
                "\n", i, argv[i], argv[0] );
        }
    }

    ConsoleSetPrio( prio );

    if( NULL != searchPath )
        mainClass->SetSearchPath( searchPath );
    else
        mainClass->SetSearchPath( "." );

    mainClass->ConnectToNetworkManager();
    mainClass->GetConnectionListFromNetworkManager();

    if( disableMenu || !isatty( fileno( stdin ) ) )
    {
        while( true )
            sleep( 1 );
    }
    else
    {
        PrintMenu();

        while( true )
        {
            int c = getchar();
            fflush( stdin );
            switch( c )
            {
            case 'M':
            case 'm':
                PrintMenu();
                break;

            case 'R':
            case 'r':
                mainClass->ToggleStatisticsPrint();
                break;

            case 'S':
            case 's':
                mainClass->CreateSampleStreams( 4 );
                break;

            case 'X':
            case 'x':
                CVodHandler::DestroyInstance();
                ConsolePrintf( PRIO_HIGH, BLUE"========== Video On Demand Server End =========="RESETCOLOR"\n" );
                ConsoleDeinit();
                return 0;
            default:
                break;
            }
            usleep( 10000 ); //Avoid high CPU load, if terminal is disconnected (running as daemon)
        }
    }
}
