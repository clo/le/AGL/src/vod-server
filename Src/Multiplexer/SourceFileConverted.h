/*
 * Video On Demand Samples
 *
 * Copyright (C) 2016 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CSourceFileConverted class.
 */
/*----------------------------------------------------------*/
#ifndef _SOURCEFILECONVERTED_H_
#define _SOURCEFILECONVERTED_H_
#include <stdio.h>
#include "Source.h"


/*----------------------------------------------------------*/
/*! \brief source streaming a file from HDD, using avconv
 */
/*----------------------------------------------------------*/
class CSourceFileConverted : public CSource
{
    int             m_hFile;
    int64_t         m_nLength;                  /* file length in seconds */
    char            m_szFileName[100];
    pid_t           m_nChildPid;                /* PID of external TS muxer */

    void Start(int64_t nPos);
    void Close();

public:
    /*----------------------------------------------------------*/
    /*! \brief Constructs Source instance
    *
    *   \param szFile - name of file to stream from
         *  \param autoDestroy - true, destroys it self, when the last listener gone
     */
    /*----------------------------------------------------------*/
    CSourceFileConverted(const char *szFile, bool autoDestroy);


    /*----------------------------------------------------------*/
    /*! \brief Destructs Source instance
     */
    /*----------------------------------------------------------*/
    ~CSourceFileConverted();




    /*----------------------------------------------------------*/
    /*! \brief called periodically to fill ring buffers of all
    *         listeners
     */
    /*----------------------------------------------------------*/
    bool FillBuffer();
};



#endif