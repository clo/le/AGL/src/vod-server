/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CStreamList class.
 */
/*----------------------------------------------------------*/
#ifndef CSTREAMLIST_H
#define	CSTREAMLIST_H

#include "Source.h"



/*----------------------------------------------------------*/
/*! \brief Aggregator class to access all available stream with a single call.
 */
/*----------------------------------------------------------*/
class CStreamList
{
    static const uint32_t MAX_STREAM_LIST_LEN = 30;
    static uint32_t m_nInstCnt;
    uint32_t m_nInst;
    CStream m_Stream[MAX_STREAM_LIST_LEN];
    uint8_t m_nPatVersion;
    uint8_t m_nPatCont;
    uint32_t m_nNextStreamSending;
    

    void GetPat(uint8_t *pTs);

public:
    /*----------------------------------------------------------*/
    /*! \brief construct a list of streams
     */
    /*----------------------------------------------------------*/
    CStreamList();



    /*----------------------------------------------------------*/
    /*! \brief Read from all streams' HDD to fill ring buffer
     *
     * \return true, if something was read from any file
     */
    /*----------------------------------------------------------*/
    bool ReadHdd();




    /*----------------------------------------------------------*/
    /*! \brief sets the same source for all active streams
     *
     * \param pSource - source object to stream
     */
    /*----------------------------------------------------------*/
    void SetSource(CSource *pSource);


    /*----------------------------------------------------------*/
    /*! \brief fills a packet with the PAT information.
     *  \note this method, writes only PAT every 30ms. Otherwise nothing is done.
     *
     * \return true, if the PAT was written to the buffer. false, because of the 30ms rule, there was no data stored.
    */
    /*----------------------------------------------------------*/
    bool GetPatPacket(uint8_t *pTs);
    
    

    /*----------------------------------------------------------*/
    /*! \brief fills a packet with packets coming from the 
     *         streams.
     *
     * \return true, if packet was available
    */
    /*----------------------------------------------------------*/
    bool GetTsPacket(uint8_t *pTs);




    /*----------------------------------------------------------*/
    /*! \brief get bytes read from all streams
     *
     * \return total bytes read
     */
    /*----------------------------------------------------------*/
    uint32_t GetBytesRead();




    /*----------------------------------------------------------*/
    /*! \brief get amount of bytes sent by video streams from all streams
     *  \note this function does not count the stuffed dummy packets.
     *
     * \return total bytes sent
     */
    /*----------------------------------------------------------*/
    uint32_t GetBytesSent();




    /*----------------------------------------------------------*/
    /*! \brief get PCR errors from all streams
     *
     * \return total bytes read
     */
    /*----------------------------------------------------------*/
    uint32_t GetErrPcr();




    /*----------------------------------------------------------*/
    /*! \brief get PCR errors from all streams
     *
     * \return total bytes read
     */
    /*----------------------------------------------------------*/
    uint32_t GetErrBufUnderflow();




    /*----------------------------------------------------------*/
    /*! \brief get PCR errors from all streams
     *
     * \return total bytes read
     */
    /*----------------------------------------------------------*/
    uint32_t GetErrTs();




    /*----------------------------------------------------------*/
    /*! \brief finds a stream with the given MAC address
     *  \param pMacAddr - Pointer to the MAC address instance to search for.
     *  \return pointer to found stream or NULL if not found
     */
    /*----------------------------------------------------------*/
    CStream *GetStream(CMacAddr *pMacAddr);
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief prints list of stream to console
     */
    /*----------------------------------------------------------*/
    void PrintStreamList();
};

#endif

