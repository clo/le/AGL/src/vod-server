/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CThreadReadHdd class.
 */
/*----------------------------------------------------------*/
#ifndef THREADREADHDD_H
#define	THREADREADHDD_H

#include <unistd.h>
#include "SafeVector.h"
#include <pthread.h>
#include <assert.h>
#include "Console.h"
#include "StreamList.h"



/*----------------------------------------------------------*/
/*! \brief Class creating a thread to call stream's reading function.
 */
/*----------------------------------------------------------*/
class CThreadReadHdd
{
private:
    pthread_t m_hThread;
    bool m_bRunning;
    CStreamList *m_StreamList;

public:    
    
    CThreadReadHdd() : m_hThread(0), m_bRunning(false), m_StreamList(NULL)
    {
    }
    
    
    /*----------------------------------------------------------*/
    /*! \brief thread reading from all stream lists
     */
    /*----------------------------------------------------------*/
    static void *Run(void *pInst) 
    {
        assert(NULL != pInst);
        
        CThreadReadHdd *that = (CThreadReadHdd *)pInst;
        /* set thread to max priority: */
        int ret;
        pthread_t this_thread = pthread_self();
        struct sched_param params;
        params.sched_priority = sched_get_priority_max(SCHED_FIFO);
        ret = pthread_setschedparam(this_thread, SCHED_FIFO, &params);
        if (ret != 0) {
            ConsolePrintf( PRIO_ERROR, "ThreadReadHdd_setschedparam(SCHED_FIFO) failed\n");
        }
        // Now verify the change in thread priority
        int policy = 0;
        ret = pthread_getschedparam(this_thread, &policy, &params);
        if (ret != 0) {
            ConsolePrintf( PRIO_ERROR, "ThreadReadHdd_getschedparam() failed\n");
        }
        
        while (that->m_bRunning)
        {
            if ( !that->m_StreamList->ReadHdd() )           // buffer full?
                usleep(50000);                              // we have 300ms buffer size, so it's save to sleep 50ms
        }
        pthread_exit(0);
    }
    
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief start reading thread
     */
    /*----------------------------------------------------------*/
    void Start()
    {
        if ( m_bRunning )
            return;
            
        if ( NULL == m_StreamList )
            return;
            
        struct sched_param param;
        pthread_attr_t tattr;
        pthread_attr_init(&tattr);
        pthread_attr_getschedparam(&tattr, &param);
        pthread_attr_setschedpolicy(&tattr, SCHED_RR);
        param.sched_priority = sched_get_priority_max(SCHED_RR);
        pthread_attr_setschedparam(&tattr, &param);
        m_bRunning = true;
        if ( 0 != pthread_create(&m_hThread, &tattr, Run, this) )
            ConsolePrintf( PRIO_ERROR, RED"! failed to create thread"RESETCOLOR"\n");
    }
    
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief stop reading thread
     */
    /*----------------------------------------------------------*/
    void Stop()
    {
        if ( m_bRunning )
        {
            m_bRunning = false;
            void *pVal;
            pthread_join(m_hThread, &pVal);
            m_hThread = 0;
        }        
    }
    
public:
    /*----------------------------------------------------------*/
    /*! \brief add a stream list to read thread
     *  \param StreamList - stream list to add for reading
     *  \return true, if stream list was added successfully
     */
    /*----------------------------------------------------------*/
    void AddStreamList(CStreamList *streamList)
    {
        Stop();
        m_StreamList = streamList;
        Start();
    }
    
    
    
    
    /*----------------------------------------------------------*/
    /*! \brief remove a stream list from read thread
     */
    /*----------------------------------------------------------*/
    void RemoveStreamList(CStreamList *streamList)
    {
        Stop();
        m_StreamList = NULL;
    }
};
    
    
    
    


#endif

