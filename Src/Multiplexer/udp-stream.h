/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

 /*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the a simple C-API to send a UDP stream.
 */
/*----------------------------------------------------------*/
#ifndef UDP_STREAM_H
#define UDP_STREAM_H

#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C"
{
#endif

/*----------------------------------------------------------*/
/*! \brief Initializies the UDP socket.
 *  \note  Do not call any other function of this component, before calling this function.
 *  \param targetIp - IP address of remote node as zero terminated string.
 *  \param targetPort - Port number of remote node.
 *  \return True, if successful, false otherwise.
 */
/*----------------------------------------------------------*/
bool UdpStream_Init( const char *targetIp, int targetPort );

/*----------------------------------------------------------*/
/*! \brief Deinitializies the UDP socket.
 *  \note  Do not call any other function of this component, after calling this function.
 */
/*----------------------------------------------------------*/
void UdpStream_Close();

/*----------------------------------------------------------*/
/*! \brief Send the given Data via the UDP socket.
 *  \param data - Byte array to be sent.
 *  \param length - The length of the Byte array.
 *  \return True, if successful, false otherwise.
 */
/*----------------------------------------------------------*/
bool UdpStream_Send( const uint8_t *data, uint32_t length );

#ifdef __cplusplus
}
#endif

#endif //UDP_STREAM_H
