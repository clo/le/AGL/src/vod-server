/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the EPDataType_t, EPDirection_t, MlbPortSpeed_t, V1I2SPortClkDriveMode_t, V1I2SStreamingDataFormat_t,
 *          V1I2SPin_t, V3I2SPortOption_t, V3I2SClockMode_t, V3I2SDelayMode_t, V3I2SPortSpeed_t, V3I2SAlignment_t, V3I2SPin_t type definition.
 */
/*----------------------------------------------------------*/
#ifndef _TYPES_H_
#define _TYPES_H_

#include <inttypes.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include "Console.h"

#define LOG(x)  ConsolePrintf(PRIO_ERROR, RED"! %s, %u, %s"RESETCOLOR"\n",x,__LINE__,__FILE__)

#define SCM_IN                              0x00
#define SCM_OUT                             0x01

typedef uint32_t TChannelId;
typedef uint16_t TDeviceId;
typedef uint8_t TNodePos;
typedef uint16_t TNodeAddr;
typedef uint32_t TBlockwidth;
typedef uint8_t TMostInstace;
typedef uint16_t TPos;               ///Value is in per mile. (0..1000)

typedef enum AimType_tag
{
    AIM_UNKNOWN,
    AIM_CDEV,
    AIM_AUDIO,
    AIM_V4L
} AimType_t;

///Data type enum values are matched to INIC API V1 & V2
typedef enum
{
    EP_Synchron = 0x00,
    EP_Asynchron = 0x01,
    EP_Control = 0x02,
    EP_Isochron = 0x03,
    EP_Unused = 0xFE,
    EP_Unknown = 0xFF
} EPDataType_t;

///Direction type enum values are matched to INIC API V1 & V2
typedef enum
{
    EPDIR_IN = 0x00,
    EPDIR_OUT = 0x01,
    EPDIR_Unknown = 0xFF
} EPDirection_t;

///INIC API V1 and V2: MLB Port speed in mulitple of MOST base clock (FS)
typedef enum
{
    MlbSpeed256FS = 0,
    MlbSpeed512FS = 1,
    MlbSpeed1024FS = 2,
    MlbSpeed2048FS = 3,
    MlbSpeed3072FS = 4,
    MlbSpeed4096FS = 5,
    MlbSpeed6144FS = 6,
    MlbSpeed8192FS = 7,
    MlbSpeedNotSet = 0xFF
} MlbPortSpeed_t;

///TSI Port ID
typedef enum
{
    V1TsiPortInstance0 = 0x5,
    V1TsiPortInstance1 = 0x25,        
    V1TsiPortInstanceNotSet = 0xFF
} V1TsiPortInstance_t;

///TSI Port Mode
typedef enum
{
    V1TsiPortModeSlave = 0x0,
    V1TsiPortModeMaster = 0x1,
    V1TsiPortModeNotSet = 0xFF
} V1TsiPortMode;

///I2S Port drive mode
typedef enum
{
    V1I2sClockModeOutput = 0,
    V1I2sClockModeInput = 1,
    V1I2sClockModeIsocOutput = 2,
    V1I2sClockModeIsocInput = 3,
    V1I2sClockModeUnknown = 0xFE,
} V1I2SPortClkDriveMode_t;

///I2S Streaming Port mode
typedef enum
{
    V1I2sStreamingPortInOut = 0,
    V1I2sStreamingPortDualIn = 1,
    V1I2sStreamingPortDualOut = 2,
    V1I2sStreamingPortFullStream = 3,
    V1I2sStreamingPortUnknown = 0xFE,
    V1I2sStreamingPortWildcard = 0xFF   ///Only valid for OS81110
} V1I2SStreamingPortMode_t;

///I2S Port streaming data format
typedef enum
{
    V1I2sDelay64Fs16Bit = 0x00,
    V1I2sDelay64Fs24Bit = 0x01,
    V1I2sDelay64FsSeq = 0x04,
    V1I2sDelay128FsSeq = 0x05,
    V1I2sDelay256FsSeq = 0x06,
    V1I2sRight64Fs16Bit = 0x07,
    V1I2sRight64Fs24Bit = 0x08,
    V1I2sSeq64Fs = 0x0B,
    V1I2sSeq128Fs = 0x0C,
    V1I2sSeq256Fs = 0x0D,
    V1I2sLeft64Fs16Bit = 0x0E,
    V1I2sLeft64Fs24Bit = 0x0F,
    V1I2sDelay512FsSeq = 0x17,
    V1I2sSeq512Fs = 0x18,
    V1I2sDataFormatUnknown = 0xFE,
} V1I2SStreamingDataFormat_t;

///I2S Stream Pin ID
typedef enum
{
    V1I2sSR0Pin = 0x00,
    V1I2sSX0Pin = 0x01,
    V1I2sSR1Pin = 0x02,
    V1I2sSX1Pin = 0x03,
    V1I2sSR2Pin = 0x04,
    V1I2sSX2Pin = 0x05,
    V1I2sSR3Pin = 0x06,
    V1I2sSX3Pin = 0x07,
    V1I2sFSYSCKPins = 0x10,
    V1I2sInvalidPin = 0xFE,
} V1I2SPin_t;

///I2S Port option
typedef enum
{
    V3I2SOptionInOut = 0,
    V3I2SOptionDualIn = 1,
    V3I2SOptionDualOut = 2,
    V3I2SOptionUnknown = 0xFE
} V3I2SPortOption_t;

///I2S Clock mode
typedef enum
{
    V3I2SClockModeOutput = 0,
    V3I2SClockModeInput = 1,
    V3I2SClockModeWildcard = 0xFF,
    V3I2SClockModeUnknown = 0xFE
} V3I2SClockMode_t;

///I2S Delay option
typedef enum
{
    V3I2SDelayOff = 0,
    V3I2SDelayEnabled = 1,
    V3I2SDelayWildcard = 0xFF,
    V3I2SDelayUnknown = 0xFE
} V3I2SDelayMode_t;

///I2S Port speed in mulitple of MOST base clock (FS)
typedef enum
{
    V3I2SSpeed64FS = 3,
    V3I2SSpeed128FS = 4,
    V3I2SSpeed256FS = 5,
    V3I2SSpeed512FS = 6,
    V3I2SSpeedWildcard = 0xFF,
    V3I2SSpeedUnknown = 0xFE
} V3I2SPortSpeed_t;

///I2S Data Alignment
typedef enum
{
    V3I2SAlignLeft16 = 0,
    V3I2SAlignLeft24 = 1,
    V3I2SAlignRight16 = 2,
    V3I2SAlignRight24 = 3,
    V3I2SAlignSeq = 4,
    V3I2SAlignUnknown = 0xFE
} V3I2SAlignment_t;

///I2S Stream Pin ID
typedef enum
{
    V3I2SSRXA0 = 0x0,
    V3I2SSRXA1 = 0x1,
    V3I2SSRXB0 = 0x10,
    V3I2SSRXB1 = 0x11,
    V3I2SInvalidPin = 0xFE
} V3I2SPin_t;

#endif
